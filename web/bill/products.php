<?php

require "../../app/Kernel.php";


if(!$kernel->security->isLoggedIn()){
	$kernel->redirect('session');
}

$entities = $kernel->model('product')->findAll();

$data = array(
	'products' => $entities
);
echo $kernel->twig->render('Invoice/products.html.twig', $data);

