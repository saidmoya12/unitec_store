<?php

require "../../app/Kernel.php";


if(!$kernel->security->isLoggedIn()){
	$kernel->redirect('session');
}

$action = $_GET['action'];
$entity	= null;
$entityDetails = array();

switch ($action){
	case 'create':
		$entity = array(
			'id'	=> null,
		);
		break;
	case 'edit':
		$id = $_GET['id'];
		
		$entity = $kernel->model('bill')->findOneBy(array('id' => $id));
		$details = $kernel->model('bill_detail')->findBy(array('bill' => $id));
		
		foreach ($details as $key => $det){
			$entityDetails[$key] = $det;
			$entityDetails[$key]['product'] = $kernel->model('product')->findOneBy(array('id' => $det['product']));
		}		
		break;
}

$products = $kernel->model('product')->findAll();
$clients = $kernel->model('client')->findAll();

$data = array(
	'bill' 			=> $entity,
	'billDetails'	=> $entityDetails,
	'products'		=> $products,
	'clients'		=> $clients,
);

echo $kernel->twig->render('Invoice/crud.html.twig', $data);