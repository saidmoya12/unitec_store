<?php

require "../../app/Kernel.php";


if(!$kernel->security->isLoggedIn()){
	$kernel->redirect('session');
}

$entities = $kernel->model('bill')->findAll();

$data = array(
	'bills' => $entities
);
echo $kernel->twig->render('Invoice/index.html.twig', $data);

