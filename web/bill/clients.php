<?php

require "../../app/Kernel.php";


if(!$kernel->security->isLoggedIn()){
	$kernel->redirect('session');
}

$entities = $kernel->model('client')->findAll();

$data = array(
	'clients' => $entities
);
echo $kernel->twig->render('Invoice/clients.html.twig', $data);

