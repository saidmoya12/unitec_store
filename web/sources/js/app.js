$(document).ready(function(){
	productCrud();
	billCrud();
});



var productCrud = function(){
	$('.product_reg .deleteBtn').on('click', function(){
		var conf = confirm("Desea eliminar el producto?");
		if(conf){
			window.location.href=$(this).attr('data-path');
		}
	});
}

var billCrud = function(){
	$('#billsPage .deleteBtn').on('click', function(){
		var conf = confirm("Desea eliminar la factura?");
		if(conf){
			window.location.href=$(this).attr('data-path');
		}
	});
	$('#billsPage .editBtn').on('click', function(){
		window.location.href=$(this).attr('data-path');
	});
	
	$('#billPage #newProductBill').on('click', function(){
		$quantity = $(this).closest('.product_new').find('#newProdCant');
		$product = $(this).closest('.product_new').find('#newProductProd');
		
		if($quantity.val().length == 0){
			$quantity.css('border-color', 'red')
			return;
		}
		
		if($product.val().length == 0){
			$product.css('border-color', 'red')
			return;
		}
		
		$quantity.css('border-color', 'initial')
		$product.css('border-color', 'initial')

		var product = JSON.parse($product.find('option:selected').attr('data-entity'))
		var index = $('#billPage #productList').children().length-1;
		
		$inputCant = $('<input name=productForm['+index+'][quantity] type="number" style="width: 100px;" value='+$quantity.val()+'>');
		$clonedProd = $product.clone();
		$clonedProd.attr('name', 'productForm['+index+'][product]')
		$clonedProd.val(product.id)
		
		$row = $('<div class="table-row">');
		
		$('<div class="table-cell">')
			.appendTo($row)
			.append($inputCant);
		$('<div class="table-cell">')
			.appendTo($row)
			.append($clonedProd);
		$('<div class="table-cell">')
			.appendTo($row)
			.append(product.value);
		$('<div class="table-cell">')
			.appendTo($row)
			.append($inputCant.val()*product.value);
		
		
		$('#billPage #productList').append($row);
	});
	
	$('#billPage form').on('submit', function(e){
		if($('#billPage #productList').children().length == 1){
			e.preventDefault();
			alert('Debe contar con productos en la lista');
		}		
	})
}