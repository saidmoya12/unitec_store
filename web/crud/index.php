<?php

require "../../app/Kernel.php";

if(!$kernel->security->isLoggedIn()){
	$kernel->redirect('session');
}

$requestData = $id = null;
$action = $_GET['action'];
$entityName = $_GET['entityName'];

if(isset($_POST['form'])){
	$requestData = $_POST['form'];
}

if(isset($_GET['id'])){
	$id = $_GET['id'];
}

if($entityName=='bill'){
	$productForms = $_POST['productForm'];
	
	switch ($action){
		case 'create':
			$requestData['user'] = $kernel->security->getUser()['id'];
			//$requestData['date'] = new DateTime($requestData['date']);
			var_dump($requestData);
			var_dump($productForms);
			
			$kernel->model($entityName)
				->insert($requestData);
			
			$billId = $kernel->model('bill')->lastInsertId();
			
			foreach ($productForms as $product){
				$product['bill'] = $billId;
				
				$kernel->model('bill_detail')
					->insert($product);
			}
			break;
		case 'update':
			$detailsUpdate = $_POST['detailsUpdate'];
			
			$kernel->model($entityName)
				->update($requestData, array('id' => $id));
			
			foreach ($detailsUpdate as $key => $detailId){
				$kernel->model('bill_detail')
					->update($productForms[$key], array('id' => $detailId));
			}
			
			break;
		case 'delete':
			$kernel->model($entityName)->delete(array('id' => $id));
			break;
		default:
			break;
	}
	
	$kernel->redirect('../bill');
	
}

try{
	switch ($action){
		case 'create':
			$kernel->model($entityName)
				->insert($requestData);
			break;
		case 'update':
			$kernel->model($entityName)
				->update($requestData, array('id' => $id));
			break;
		case 'delete':
			$kernel->model($entityName)->delete(array('id' => $id));
			break;
		default:
			$kernel->redirect('error.php');
	}
}catch (Exception $e){
	$kernel->redirect('error.php?error='.$e->getMessage());
}

$kernel->redirect($_SERVER['HTTP_REFERER']);