<?php

require "../../app/Kernel.php";

$data = array();

if(isset($_POST['email'])){
	if($kernel->security->login(array(
		'username' => $_POST['email'], 'password' => $_POST['password']
	))){
		$kernel->redirect('../index.php');
	}
	
	$data['error'] = 'Username or password wrong';
}else{
	$kernel->security->logout();
}

echo $kernel->twig->render('Session/login.html.twig', $data);