<?php

require "../app/Kernel.php";

$data = array();

if(!$kernel->security->isLoggedIn()){
	$kernel->redirect('session');
}

echo $kernel->twig->render('Home/index.html.twig', $data);