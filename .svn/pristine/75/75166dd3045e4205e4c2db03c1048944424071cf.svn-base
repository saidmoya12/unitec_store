<?php

class Model extends \PDO{

	private $entity;

	/**
	 * Find by
	 * @param string $criteria
	 * @param integer $offset
	 * @param array $options
	 * 
	 * @return array
	 */
	public function findOneBy(array $criteria, $offset=0, array $options = array()){
		return $this->findBy($criteria, $offset, 1, $options);
	}

	/**
	 * Find by
	 * @param array 	$criteria
	 * @param integer	$offset
	 * @param integer	$limit
	 * @param array 	$options
	 * 
	 * @return array
	 */
	public function findBy(array $criteria, $offset=0, $limit=null, array $options = array()){
		$options = array_merge(array(
			'orderBy'		=> null,
			'conditional' 	=> 'AND' //OR
		), $options);

		$sql = 'SELECT * FROM `'.$this->entity.'` e';
		
		if(!empty($criteria)){
			$sql .= ' WHERE ';

			$i=0;
			foreach ($criteria as $key => $value) {
				if($i>0){ $sql .= ' '.$options['conditional'].' '; }
				$i++;
				
				if(is_null($value)){
					$sql .= 'e.`'.$key.'` is NULL';
					unset($criteria[$key]);
					continue;
				}

				$sql .= 'e.`'.$key.'` = '.':'.$key;
			}
		}
		
		if(isset($options['orderBy'])){
			$sql .= ' ORDER BY ';
			$i=0;
			foreach ($options['orderBy'] as $key => $value) {
				if($i>0){ $sql .= ', '; }
				$i++;
				$sql .= 'e.`'.$key.'` '.$value;
			}
		}

		if(!$limit){
			if($offset>0){
				$sql.=' LIMIT '.$offset.', '.'18446744073709551615';
			}
		}else{
			$sql.=' LIMIT '.$offset.', '.$limit;
		}
		
		$stmt = $this->prepare($sql);
		if(!$stmt){
			$err = $this->errorInfo();
			throw new \PDOException($err[1].' on '.$sql, 1);
		}

		reset($criteria);
		foreach ($criteria as $key => $value) {
			$stmt->bindValue(':'.$key, $value);
		}

		$stmt->execute();

		if($limit==1){
			return $stmt->fetch(PDO::FETCH_ASSOC);
		}

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * @return array
	 */
	public function findAll(){
		$sql = 'SELECT * FROM `'.$this->entity.'` e';

		$stmt = $this->prepare($sql);
		if(!$stmt){
			$err = $this->errorInfo();
			throw new \PDOException('Error '.$err[2].' '.$err[1].' on '.$sql, true);
		}

		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * @param array $data
	 */
	public function insert(array $data){
		$sql = 'INSERT INTO `'.$this->entity.'` ';


		$sql.=' ('.implode(",", (array_keys($data))).') VALUES('.implode(",", array_map(function($a){
			return ':'.$a;
		}, array_keys($data))).')';

		$stmt = $this->prepare($sql);

		if(!$stmt){
			$err = $this->errorInfo();
			throw new \PDOException('Error '.$err[2].': '.$err[1].' on '.$sql, 1);
		}

		reset($data);
		foreach ($data as $key => $value) {
			$stmt->bindValue(':'.$key, $value);
		}

		return $stmt->execute();
	}

	/**
	 * @param array $data
	 * @param array $criteria
	 */
	public function update(array $data, array $criteria, array $options = array()){
		$options = array_merge(array(
			'conditional' => 'AND'
		), $options);


		$sql = 'UPDATE `'.$this->entity.'` SET ';

		$i=0;
		foreach ($data as $key => $value) {
			if($i>0){ $sql .= ', ';}
			$i++;
			$sql .= '`'.$key.'` = :'.$key;
		}

		if(!empty($criteria)){
			$sql .= ' WHERE ';

			$i=0;
			foreach ($criteria as $key => $value) {
				if($i>0){ $sql .= ' '.$options['conditional'].' ';}
				$i++;

				if(is_array($value)){
					foreach ($value as $kv => $v) {
						$sql .= $kv>1 ? ' OR ' : '';

						$sql .= '`'.$key.'` = :c_'.$key.'_'.$kv;
					}
					continue;
				}

				$sql .= '`'.$key.'` = :c_'.$key;

			}
		}
		
		$stmt = $this->prepare($sql);
		if(!$stmt){
			$err = $this->errorInfo();
			throw new \PDOException('Error '.$err[2].' '.$err[1].' on '.$sql, 1);
		}

		if(isset($criteria)){
			reset($criteria);
			foreach ($criteria as $key => $value) {
				if(is_array($value)){
					foreach ($value as $kv => $v) {
						$stmt->bindValue(':c_'.$key.'_'.$kv, $v);
					}
					continue;
				}

				$stmt->bindValue(':c_'.$key, $value);
			}
		}

		reset($data);
		foreach ($data as $key => $value) {
			if(is_bool($value)){
				$value = $value ? 1 : 0;
			}

			$stmt->bindValue(':'.$key, $value);
		}

		try{
			return $stmt->execute();
		}catch(PDOException $e){
			throw new \Exception($e->getMessage(), 1);
		}

	}

	/**
	 * @param array $criteria
	 * @param array $options = array('conditional'=>'and)
	 */
	public function delete(array $criteria, array $options=array()){
		$options = array_merge(array(
			'conditional' 	=> 'AND' //OR
		), $options);

		$sql = 'DELETE FROM `'.$this->entity.'`';

		$sql .= ' WHERE ';

		$i=0;
		foreach ($criteria as $key => $value) {
			if($i>0){ $sql .= ' '.$options['conditional'].' '; }
			$i++;

			$sql .= '`'.$key.'` = '.':'.$key;
		}


		$stmt = $this->prepare($sql);
		if(!$stmt){
			$err = $this->errorInfo();
			throw new \PDOException('Error '.$err[2].' '.$err[1].' on '.$sql, 1);
		}

		reset($criteria);
		foreach ($criteria as $key => $value) {
			$stmt->bindValue(':'.$key, $value);
		}

		return $stmt->execute();
	} 

	/**
	 * mysql real scape
	 */
	private function mres($value){
		$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
		$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

		return str_replace($search, $replace, $value);
	}	

	/**
	 * @param string $entity
	 */
	public function __construct($entity){
		$this->entity = $this->mres($entity);

		parent::__construct('sqlite:'.__DIR__.'/../src/model/ekoofiles.s3db', null, null);
	}

	/**
	 * @param string $seqname[optional]
	 * 
	 * @return integer
	 */
	public function lastInsertId($seqname = NULL){
		if($name == null){
			$name = $this->entity;
		}

		return parent::lastInsertId($name);
	}
}
