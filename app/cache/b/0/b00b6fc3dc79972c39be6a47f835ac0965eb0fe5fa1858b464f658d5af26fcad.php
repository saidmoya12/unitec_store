<?php

/* macros.twig */
class __TwigTemplate_ec347e8a0fd0ca5ce83ae03ad3e4b870d79d5effcdccc25c848fa51c70a3785a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "
";
    }

    // line 2
    public function getlinkCSS($__url__ = null, $__asset__ = null, $__attr__ = null)
    {
        $context = $this->env->mergeGlobals(array(
            "url" => $__url__,
            "asset" => $__asset__,
            "attr" => $__attr__,
            "varargs" => func_num_args() > 3 ? array_slice(func_get_args(), 3) : array(),
        ));

        $blocks = array();

        ob_start();
        try {
            echo "<link ";
            echo ((array_key_exists("attr", $context)) ? (_twig_default_filter((isset($context["attr"]) ? $context["attr"] : null), "")) : (""));
            echo " rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, ((((array_key_exists("asset", $context)) ? (_twig_default_filter((isset($context["asset"]) ? $context["asset"] : null), true)) : (true))) ? (call_user_func_array($this->env->getFunction('asset')->getCallable(), array((isset($context["url"]) ? $context["url"] : null)))) : ((isset($context["url"]) ? $context["url"] : null))), "html", null, true);
            echo "\">";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 5
    public function getincludeJS($__url__ = null, $__asset__ = null, $__attr__ = null)
    {
        $context = $this->env->mergeGlobals(array(
            "url" => $__url__,
            "asset" => $__asset__,
            "attr" => $__attr__,
            "varargs" => func_num_args() > 3 ? array_slice(func_get_args(), 3) : array(),
        ));

        $blocks = array();

        ob_start();
        try {
            echo "<script ";
            echo ((array_key_exists("attr", $context)) ? (_twig_default_filter((isset($context["attr"]) ? $context["attr"] : null), "")) : (""));
            echo " type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, ((((array_key_exists("asset", $context)) ? (_twig_default_filter((isset($context["asset"]) ? $context["asset"] : null), true)) : (true))) ? (call_user_func_array($this->env->getFunction('asset')->getCallable(), array((isset($context["url"]) ? $context["url"] : null)))) : ((isset($context["url"]) ? $context["url"] : null))), "html", null, true);
            echo "\"></script>";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "macros.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 5,  24 => 2,  19 => 3,);
    }
}
/* {# link css #}*/
/* {% macro linkCSS(url, asset, attr) %}<link {{ attr|default('')|raw }} rel="stylesheet" type="text/css" href="{{ asset|default(true) ? asset(url) : url }}">{% endmacro %}*/
/* */
/* {#include js#}*/
/* {% macro includeJS(url, asset, attr) %}<script {{attr|default('')|raw}} type="text/javascript" src="{{ asset|default(true) ? asset(url) : url }}"></script>{% endmacro %}*/
