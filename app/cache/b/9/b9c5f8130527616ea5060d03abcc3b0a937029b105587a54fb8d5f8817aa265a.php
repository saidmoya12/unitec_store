<?php

/* base.html.twig */
class __TwigTemplate_cbf3201e7b4437acde0a6d80ea19f65581b9403de545d1756de5c4876c705017 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'header_btn' => array($this, 'block_header_btn'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["macro"] = $this->loadTemplate("macros.twig", "base.html.twig", 1);
        // line 2
        ob_start();
        // line 3
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<meta charset=\"utf-8\">
    \t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
   \t \t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t";
        // line 9
        $this->displayBlock('head', $context, $blocks);
        // line 10
        echo "\t\t<title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t\t";
        // line 11
        echo $context["macro"]->getlinkCSS("sources/js/lib/jquery-ui/jquery-ui.css");
        echo "
\t\t";
        // line 12
        echo $context["macro"]->getlinkCSS("sources/js/lib/jquery-ui/jquery-ui.css");
        echo "
\t\t";
        // line 13
        echo $context["macro"]->getlinkCSS("sources/css/bootstrap.min.css");
        echo "
\t\t";
        // line 14
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "\t</head>
\t<body>
\t\t<div id=\"container\">";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 21
        echo "\t\t</div>
\t\t<div id=\"scripts\">
\t\t\t";
        // line 23
        echo $context["macro"]->getincludeJS("sources/js/jquery.js", true, "defer");
        echo "
\t\t\t";
        // line 24
        echo $context["macro"]->getincludeJS("sources/js/lib/jquery-ui/jquery-ui.js", true, "defer");
        echo "
\t\t\t";
        // line 25
        echo $context["macro"]->getincludeJS("sources/js/lib/jquery_noty/jquery_noty.js", true, "defer");
        echo "
\t\t\t";
        // line 26
        echo $context["macro"]->getincludeJS("sources/js/bootstrap.min.js", true, "defer");
        echo "
\t\t\t";
        // line 27
        echo $context["macro"]->getincludeJS("sources/js/lib/jquery-validation/jquery.validate.js", true, "defer");
        echo "
\t\t\t";
        // line 28
        echo $context["macro"]->getincludeJS("sources/js/lib/jquery-validation/jquery.form.js", true, "defer");
        echo "
\t\t\t";
        // line 29
        $this->displayBlock('javascripts', $context, $blocks);
        // line 30
        echo "\t\t</div>
\t</body>
</html>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 9
    public function block_head($context, array $blocks = array())
    {
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        echo "Unitec Store";
    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        // line 18
        echo "\t\t\t<div id=\"header\">";
        $this->displayBlock('header_btn', $context, $blocks);
        echo "</div>
\t\t\t<div id=\"content\">";
        // line 19
        $this->displayBlock('content', $context, $blocks);
        echo "</div>
\t\t";
    }

    // line 18
    public function block_header_btn($context, array $blocks = array())
    {
    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
    }

    // line 29
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 29,  138 => 19,  133 => 18,  127 => 19,  122 => 18,  119 => 17,  114 => 14,  108 => 10,  103 => 9,  95 => 30,  93 => 29,  89 => 28,  85 => 27,  81 => 26,  77 => 25,  73 => 24,  69 => 23,  65 => 21,  63 => 17,  59 => 15,  57 => 14,  53 => 13,  49 => 12,  45 => 11,  40 => 10,  38 => 9,  30 => 3,  28 => 2,  26 => 1,);
    }
}
/* {% import 'macros.twig' as macro %}*/
/* {% spaceless %}*/
/* <!DOCTYPE html>*/
/* <html>*/
/* 	<head>*/
/* 		<meta charset="utf-8">*/
/*     	<meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*    	 	<meta name="viewport" content="width=device-width, initial-scale=1">*/
/* 		{% block head %}{% endblock %}*/
/* 		<title>{% block title %}Unitec Store{% endblock %}</title>*/
/* 		{{ macro.linkCSS('sources/js/lib/jquery-ui/jquery-ui.css') }}*/
/* 		{{ macro.linkCSS('sources/js/lib/jquery-ui/jquery-ui.css') }}*/
/* 		{{ macro.linkCSS('sources/css/bootstrap.min.css') }}*/
/* 		{% block stylesheets %}{% endblock %}*/
/* 	</head>*/
/* 	<body>*/
/* 		<div id="container">{% block body %}*/
/* 			<div id="header">{% block header_btn %}{% endblock %}</div>*/
/* 			<div id="content">{% block content %}{% endblock %}</div>*/
/* 		{% endblock %}*/
/* 		</div>*/
/* 		<div id="scripts">*/
/* 			{{ macro.includeJS('sources/js/jquery.js', true, 'defer') }}*/
/* 			{{ macro.includeJS('sources/js/lib/jquery-ui/jquery-ui.js', true, 'defer') }}*/
/* 			{{ macro.includeJS('sources/js/lib/jquery_noty/jquery_noty.js', true, 'defer') }}*/
/* 			{{ macro.includeJS('sources/js/bootstrap.min.js', true, 'defer') }}*/
/* 			{{ macro.includeJS('sources/js/lib/jquery-validation/jquery.validate.js', true, 'defer') }}*/
/* 			{{ macro.includeJS('sources/js/lib/jquery-validation/jquery.form.js', true, 'defer') }}*/
/* 			{% block javascripts %}{% endblock %}*/
/* 		</div>*/
/* 	</body>*/
/* </html>*/
/* {% endspaceless %}*/
