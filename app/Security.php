<?php

class Security{
	private $kernel;
	private $options;
	private $user;
	private $KEY = '32321145';

	/**
	 * @param request $_POST
	 * 
	 * @return boolean
	 */
	public function login($request){
		$this->user = $this->kernel->model('user')->findOneBy(array(
			'email'		=> $request['username'],
		), 0, array('conditional'=>'OR'));
		
		if(!$this->user){
			return false;
		}

		$crypt = hash_hmac("md5", $request['password'], $this->user['salt'].'~'.$this->KEY);	
		
		echo $crypt;
		
		if($crypt != $this->user['password']){
			return false;
		}

		unset($this->user['password']);
		$_SESSION['user'] = $this->user;
		
		

		return true;
	}

	/**
	 * Logout session
	 */
	public function logout(){
		$_SESSION['user'] = $this->user = null;
		session_destroy();
	}

	/**
	 * @return array
	 */
	public function getUser(){
		if(!$this->user){
			return false;
		}

		return $this->user;
	}

	/**
	 * @param string role
	 * 
	 * @return boolean
	 */
	public function isGranted($role){
		if(!$this->user){
			return false;
		}

		if(!strpos($this->user['roles'], $role)){
			return false;
		}

		return true;
	}
	
	public function isLoggedIn(){
		if(!$this->user){
			return false;
		}
		return true;
	}

	/**
	 * userData in any order
	 * array(username=>'', password=>'', group=>'', roles=>array())
	 * @param array userData
	 * @return array|false
	 */
	public function createUser(array $userData){
		$user = $this->kernel->model('users')->findOneBy(array(
			'username'	=> $userData['username'],
			'email'		=> $userData['username'],
		), 0, array('conditional'=>'OR'));

		if(!empty($user)){
			throw new \Exception("The username exist", 1);
		}

		if(!isset($userData['plainPassword'])){
			throw new \Exception("plainPassword is undefined", 1);
		}

		if(!isset($userData['roles']) || !is_array($userData['roles'])){
			throw new \Exception("undefined roles", 1);
		}

		$this->setUserData($userData);
		$this->kernel->model('users')->insert($userData);
		$userData['id'] = $this->kernel->model('users')->lastInsertId();
		return $userData;
	}

	/**
	 * updateUser
	 * array $userData array('field'=>'value')
	 * array $criteria array('field'=>'value')
	 */
	public function updateUser($userData, array $criteria, $options = array()){
		if(empty($userData['plainPassword'])){
			unset($userData['plainPassword']);//no password update if empty
		}
		
		try{
			$this->setUserData($userData);
			$this->kernel->model('users')->update($userData, $criteria, $options);
		}catch(\Exception $e){
			throw $e;
		}
	}

	public function reserRequest($email){
		$user = $this->kernel->model('users')->findOneBy(array(
			'username'	=> $email,
			'email'		=> $email,
		), 0, array('conditional'=>'OR'));
		
		if(empty($user)){
			throw new \Exception('User not found with email '.$email, 1);
		}
		
		if(isset($user['reset_token_at'])){
			if(strtotime($user['reset_token_at'])>strtotime('-24 hours')){
				if($user['reset_token'] != null){
					throw new \Exception('The password was requested at last 24 hours', 1);
				}
			}
		}
		
		$group = $this->kernel->model('groups')->findOneBy(array('id'=>$user['group']));
		
		$userData = array(
			'reset_token' => hash_hmac("md5", $user['email'], time().'~'.$this->KEY),
			'reset_token_at' => date('Y-m-d H:i:s'),
		);
		
		$this->updateUser($userData, array(
			'username'	=> $user['username'],
		));
		
		$message = \Swift_Message::newInstance('Password reset')
			->setFrom(array('noreply@nodefined.com' => 'nodefined'))
			->setTo(array($user['email']))
			->setBody($this->kernel->twig->render('User/reset_mail.html.twig', array(
				'user'			=> $user,
				'group'			=> $group,
				'reset_url'		=> $this->kernel->globalAsset('reset/password_reset.php?_token='.$userData['reset_token']),
			)), 'text/html')
		;
		
		$this->kernel->mailer->send($message);
	}

	public function changePassword($user, $password){
		$user['plainPassword'] = $password;
		try{
			$this->setUserData($user);
			$this->kernel->model('users')->update($user, array('username' => $user['username']));
		}catch(\Exception $e){
			throw $e;
		}
		
		return $user;
	}

	private function setUserData(&$userData){
		if(isset($userData['roles']) && is_array($userData['roles'])){
			$userData['roles'] = '['.implode(',',$userData['roles']).']';
		}

		if(!isset($userData['salt'])){
			$userData['salt'] = md5(uniqid());
		}

		if(isset($userData['plainPassword']) && !empty($userData['plainPassword'])){
			$userData['password'] = hash_hmac("md5", $userData['plainPassword'], $userData['salt'].'~'.$this->KEY);
			unset($userData['plainPassword']);
		}
	}

	public function __construct(Kernel $kernel, array $options = array()){
		$this->options = array_merge(array(
			'loginPath'	=> '/',
		), $options);

		$this->kernel = $kernel;

		if(isset($_SESSION['user'])){
			$this->user = $_SESSION['user'];
		}
	}
}